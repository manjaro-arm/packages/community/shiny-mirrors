# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Mark Wagie <mark@manjaro.org>>

pkgname=shiny-mirrors
pkgver=r149.aeaecb3
pkgrel=1
pkgdesc="A replacement for Manjaro's pacman-mirrors, rewritten in Rust! A tool to find the best mirrors for you!"
arch=('x86_64' 'aarch64')
url="https://gitlab.com/Arisa_Snowbell/shiny-mirrors"
license=('GPL3')
depends=('gcc-libs')
makedepends=('cargo-nightly' 'git')
backup=("etc/$pkgname.conf")
_commit=aeaecb37373fa6f268627dc7afe148c5dbb55d01
source=("git+https://gitlab.com/Arisa_Snowbell/shiny-mirrors.git#commit=$_commit?signed")
sha256sums=('SKIP')
validpgpkeys=('E2C998FA1F7B651E45B20CDC56AA2C2801F619D7') # Arisa Snowbell <arisa.snowbell@gmail.com> (Hyena)
#validpgpkeys=('93F4694364C3E688BA33E3E41CBE6B7A2B054E06') # Arisa Snowbell <arisa.snowbell@gmail.com> (Laptop)

pkgver() {
  cd "$srcdir/$pkgname"
  printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  cd "$srcdir/$pkgname"
  export RUSTUP_TOOLCHAIN=nightly
  cargo fetch --locked --target "$CARCH-unknown-linux-gnu"
}

build() {
  cd "$srcdir/$pkgname"
  export RUSTUP_TOOLCHAIN=nightly
  export CARGO_TARGET_DIR=target
  cargo build --frozen --release --features manjaro --no-default-features
}

package() {
  cd "$srcdir/$pkgname"
  install -Dm755 "target/release/$pkgname" -t "$pkgdir/usr/bin/"
  install -Dm644 "target/completions/_${pkgname}" -t \
    "$pkgdir/usr/share/zsh/site-functions/"
  install -Dm644 "target/completions/$pkgname.bash" \
    "$pkgdir/usr/share/bash-completion/completions/$pkgname"
  install -Dm644 "target/completions/$pkgname.fish" -t \
    "$pkgdir/usr/share/fish/completions/"
  install -Dm644 "conf/$pkgname.conf" -t "$pkgdir/etc/"
  install -Dm644 "$pkgname/systemd/$pkgname"{.service,.timer} -t \
    "$pkgdir/usr/lib/systemd/system/"
}
